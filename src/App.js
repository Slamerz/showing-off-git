import React from "react";
import "./App.css";
import { SomeComponent } from "./SomeComponent";

function App() {
  return (
    <>
      <h1>Hello World!</h1>
      <SomeComponent />
    </>
  );
}

export default App;
